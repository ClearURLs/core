/*
 * ClearURLs
 * Copyright (c) 2017-2022 Kevin Röbert.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * Models a multimap backed by a {@link Set}.
 */
export default class Multimap<K, V> implements Iterable<[K, V]> {
    private _map: Map<K, Set<V>>
    private _size: number

    constructor() {
        this._size = 0
        this._map = new Map()
    }

    public get size(): number {
        return this._size
    }

    public get(key: K): Set<V> {
        const values = this._map.get(key)

        if (values) {
            return new Set(values)
        } else {
            return new Set()
        }
    }

    public put(key: K, value: V): boolean {
        let values = this._map.get(key)

        if (!values) {
            values = new Set()
        }

        const count = values.size
        values.add(value)

        if (values.size === count) {
            return false
        }

        this._map.set(key, values)
        this._size++

        return true
    }

    public has(key: K): boolean {
        return this._map.has(key)
    }

    public hasEntry(key: K, value: V): boolean {
        const values = this._map.get(key)

        if (!values) {
            return false
        }

        return values.has(value)
    }

    public delete(key: K): boolean {
        const values = this._map.get(key)

        if (values && this._map.delete(key)) {
            this._size -= values.size

            return true
        }

        return false
    }

    public deleteEntry(key: K, value: V): boolean {
        const values = this._map.get(key)

        if (values) {
            if (!values.delete(value)) {
                return false
            }
            this._size--

            return true
        }

        return false
    }

    public clear(): void {
        this._map.clear()
        this._size = 0
    }

    public entries(): IterableIterator<[K, V]> {
        const self = this

        function* gen(): IterableIterator<[K, V]> {
            for (const [key, values] of self._map.entries()) {
                for (const value of values) {
                    yield [key, value]
                }
            }
        }

        return gen()
    }

    public values(): IterableIterator<V> {
        const self = this

        function* gen(): IterableIterator<V> {
            for (const [, value] of self.entries()) {
                yield value
            }
        }

        return gen()
    }

    public keys(): IterableIterator<K> {
        return this._map.keys()
    }

    public forEach<T>(callback: (this: T | this, key: K, value: V, map: this) => void, thisArg?: T): void {
        for (const [key, value] of this.entries()) {
            callback.call(thisArg === undefined ? this : thisArg, key, value, this)
        }
    }

    public [Symbol.iterator]() {
        return this.entries()
    }
}
