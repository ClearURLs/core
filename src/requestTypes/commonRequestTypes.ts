/*
 * ClearURLs
 * Copyright (c) 2017-2022 Kevin Röbert.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import RequestType from './requestType'

export default class CommonRequestTypes {
    public static readonly FONT: RequestType = RequestType.of('font')
    public static readonly IMAGE: RequestType = RequestType.of('image')
    public static readonly MAIN_FRAME: RequestType = RequestType.of('main_frame')
    public static readonly MEDIA: RequestType = RequestType.of('media')
    public static readonly OBJECT: RequestType = RequestType.of('object')
    public static readonly OTHER: RequestType = RequestType.of('other')
    public static readonly SCRIPT: RequestType = RequestType.of('script')
    public static readonly STYLESHEET: RequestType = RequestType.of('stylesheet')
    public static readonly SUB_FRAME: RequestType = RequestType.of('sub_frame')
    public static readonly WEBSOCKET: RequestType = RequestType.of('websocket')
    public static readonly XMLHTTPREQUEST: RequestType = RequestType.of('xmlhttprequest')

    // Ping requests
    public static readonly PING: RequestType = RequestType.of('ping')

    protected constructor() {
        // emulated enum
    }
}
