/*
 * ClearURLs
 * Copyright (c) 2017-2022 Kevin Röbert.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import CommonRequestTypes from './commonRequestTypes'
import RequestType from './requestType'

export default class ChromeRequestTypes extends CommonRequestTypes {
    public static readonly CSP_REPORT: RequestType = RequestType.of('csp_report')

    private constructor() {
        super()
        // emulated enum
    }

    /**
     * Returns all supported {@link RequestType}'s.
     */
    public static getSupportedTypes(): ReadonlyArray<RequestType> {
        return Object.values(CommonRequestTypes).concat(Object.values(ChromeRequestTypes))
    }
}
