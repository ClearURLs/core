/*
 * ClearURLs
 * Copyright (c) 2017-2022 Kevin Röbert.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import ReplacePattern from '../src/replacePattern'

describe('ReplacePatternTest', () => {
    it('should apply correctly', () => {
        const pattern: ReplacePattern = new ReplacePattern('§1§://§2§.clearurls.xyz/index.html?foo=§3§')
        const values: string[] = ['https', 'test', 'bar']
        const expected: string = 'https://test.clearurls.xyz/index.html?foo=bar'

        expect(pattern.apply(values)).toBe(expected)
    })

    it('should replace unknown values with empty string', () => {
        const pattern: ReplacePattern = new ReplacePattern('§1§://§2§.clearurls.xyz/index.html?' +
            'foo=§3§&bla=§4§&bad=§' + Number.MAX_SAFE_INTEGER + '§')
        const values: string[] = ['https', 'test', 'bar']
        const expected: string = 'https://test.clearurls.xyz/index.html?foo=bar&bla=&bad='

        expect(pattern.apply(values)).toBe(expected)
    })

    it('should remove on empty string', () => {
        const pattern: ReplacePattern = new ReplacePattern('')
        const values: string[] = []
        const expected: string = ''

        expect(pattern.apply(values)).toBe(expected)
        expect(ReplacePattern.REMOVE.apply(values)).toBe(expected)
        expect(pattern.apply(['a', 'b', 'c'])).toBe(expected)
        expect(ReplacePattern.REMOVE.apply(['a', 'b', 'c'])).toBe(expected)
    })
})
