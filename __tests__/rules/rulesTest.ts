/*
 * ClearURLs
 * Copyright (c) 2017-2022 Kevin Röbert.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import Rule from '../../src/rules/rule'

describe('RuleTest', () => {
    const exp = '^test$'
    let rule: Rule

    it('should create correct instance', () => {
        rule = new Rule(exp)

        expect(rule.matchPattern).toStrictEqual(new RegExp(exp))
    })

    it('should return correct toString', () => {
        rule = new Rule(exp)

        expect(rule.toString()).toBe(exp)
    })

    it('should set active value correctly', () => {
        rule = new Rule(exp)

        expect(rule.isActive).toBe(true)
        rule.deactivate()
        expect(rule.isActive).toBe(false)
        rule.activate()
        expect(rule.isActive).toBe(true)
    })
})
