/*
 * ClearURLs
 * Copyright (c) 2017-2022 Kevin Röbert.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import FirefoxRequestTypes from '../../src/requestTypes/firefoxRequestTypes'

describe('FirefoxRequestTypesTest', () => {
    it('should contain all request types', () => {
        const supportedTypes = ['font', 'image', 'imageset', 'main_frame', 'media', 'object',
            'object_subrequest', 'other', 'script', 'stylesheet', 'sub_frame', 'websocket', 'xml_dtd', 'xmlhttprequest',
            'xslt', 'ping', 'beacon']

        const firefoxTypes = FirefoxRequestTypes.getSupportedTypes().map(value => value.type)

        expect(firefoxTypes.sort()).toEqual(supportedTypes.sort())
    })
})
