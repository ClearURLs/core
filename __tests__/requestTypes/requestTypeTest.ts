/*
 * ClearURLs
 * Copyright (c) 2017-2022 Kevin Röbert.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import IllegalArgumentException from '../../src/exceptions/illegalArgumentException'
import RequestType from '../../src/requestTypes/requestType'

describe('RequestTypeTest', () => {
    it('should create constant', () => {
        const type1: RequestType = RequestType.ALL
        const type2: RequestType = RequestType.of('ALL')

        expect(type1).toBe(type2)
        expect(type1.type).toBe(type2.type)
    })

    it('should intern', () => {
        const type1: RequestType = RequestType.of('test')
        const type2: RequestType = RequestType.of('test')

        expect(type1).toBe(type2)
        expect(type1.type).toBe(type2.type)
    })

    it('should be serializable', () => {
        const type: RequestType = RequestType.of('test')
        const serialized: string = JSON.stringify(type)
        const deserialized: RequestType = RequestType.fromJSON(JSON.parse(serialized))

        expect(type).toBe(deserialized)
        expect(type.type).toBe(deserialized.type)
    })

    it('should throw exception on wrong json', () => {
        expect(() => RequestType.fromJSON(JSON.parse('[]'))).toThrow(IllegalArgumentException)
        expect(() => RequestType.fromJSON(JSON.parse('{}'))).toThrow(IllegalArgumentException)
        expect(() => RequestType.fromJSON(JSON.parse('{"_type": ""}'))).toThrow(IllegalArgumentException)
    })

    it('should throw exception on invalid type', () => {
        expect(() => RequestType.of('')).toThrow(IllegalArgumentException)
    })
})
